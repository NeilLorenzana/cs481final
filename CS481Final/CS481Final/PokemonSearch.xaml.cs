﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using PokeAPI;
using CS481Final.Models;



namespace CS481Final
{
    
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PokemonSearch : ContentPage
	{
        //string oldSearch;

        public PokemonSearch ()
		{
			InitializeComponent ();
            PokemonFind.Keyboard = Keyboard.Create(KeyboardFlags.CapitalizeNone);
        }

        async void RequestPokemonData(object sender, System.EventArgs e)
        {
            if(string.IsNullOrEmpty(PokemonFind.Text))
            {
                //PokemonFind.Text = oldSearch;
                //PokemonFind.lastSearch = oldSearch;
                await DisplayAlert("Empty Search", "Pokemon search is empty.", "Close");
                System.Diagnostics.Debug.WriteLine("Empty string");

                return;
            }

            try
            {
                await PullData();
                System.Diagnostics.Debug.WriteLine("got pokemon info");
                System.Diagnostics.Debug.WriteLine("got species info");
            }
            catch(HttpRequestException ex)
            {
                System.Diagnostics.Debug.WriteLine("pokemon not found");
                await DisplayAlert("Invalid search", "Pokemon not found.", "Close");
                LoadingIcon.IsRunning = false;


            }
        }

        async Task PullData()
        {
            LoadingIcon.IsRunning = true;

            string newPokemon = PokemonFind.Text;

            Pokemon p = await DataFetcher.GetNamedApiObject<Pokemon>(newPokemon.ToLower());
            PokemonSpecies pS = await DataFetcher.GetNamedApiObject<PokemonSpecies>(newPokemon.ToLower());
            IntroLabel.IsVisible = false;

            string PokemonName = p.Name;
            int PokemonHeight = p.Height;
            int PokemonWeight = p.Mass;
            int PokemonXp = p.BaseExperience;
            int PokemonOrder = p.Order;
            float PokemonCaptureRate = pS.CaptureRate;

            PokemonCaptureRate = CalculateCaptureRate(PokemonCaptureRate);
            
            OrderLabel.Text = "#" + PokemonOrder;
            NameLabel.Text = "Name: " + PokemonName.ToUpper();
            HeightWeightLabel.Text = "Height/Weight: " + PokemonHeight.ToString() +" dm " + "/" + PokemonWeight.ToString() + " hg";
            ExpLabel.Text = "Experience on defeat: " + PokemonXp.ToString() + "XP";
            CaptureLabel.Text = "Capture rate: " + PokemonCaptureRate.ToString("00.0") + "%";


            OrderLabel.IsVisible = true;
            NameLabel.IsVisible = true;
            HeightWeightLabel.IsVisible = true;
            ExpLabel.IsVisible = true;
            CaptureLabel.IsVisible = true;



            LoadingIcon.IsRunning = false;

        }

        public float CalculateCaptureRate(float PokemonCaptureRate)
        {
            float rate;

            rate = PokemonCaptureRate / 255;
            rate = rate * 100;
            return rate;

        }
    }

}
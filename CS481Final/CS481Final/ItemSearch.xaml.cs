﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using PokeAPI;
using CS481Final.Models;



namespace CS481Final
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ItemSearch : ContentPage
    {
        //string oldSearch;

        public ItemSearch()
        {
            InitializeComponent();
            ItemFind.Keyboard = Keyboard.Create(KeyboardFlags.CapitalizeNone);
        }

        async void RequestItemData(object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(ItemFind.Text))
            {
                //PokemonFind.Text = oldSearch;
                //PokemonFind.lastSearch = oldSearch;
                await DisplayAlert("Empty Search", "Item search is empty.", "Close");
                System.Diagnostics.Debug.WriteLine("Empty string");

                return;
            }

            try
            {
                await PullData();
                System.Diagnostics.Debug.WriteLine("Item found.");
            }
            catch (HttpRequestException ex)
            {
                System.Diagnostics.Debug.WriteLine("Item not found");
                await DisplayAlert("Invalid search", "Item not found.", "Close");
                LoadingIcon.IsRunning = false;


            }
        }

        async Task PullData()
        {
            LoadingIcon.IsRunning = true;

            string newItem = ItemFind.Text;

            Item i = await DataFetcher.GetNamedApiObject<Item>(newItem.ToLower());
            IntroLabel.IsVisible = false;
            InstructionLabel.IsVisible = false;


            int ItemId = i.ID;
            string ItemName = i.Name;
            int ItemCost = i.Cost;
            int? ItemFlingPower = i.FlingPower;

            ItemIdLabel.Text = "Item #" + ItemId;
            ItemNameLabel.Text = ItemName.ToUpper();
            ItemCostLabel.Text = "Cost: " + ItemCost + " Pokedollars";
            ItemFlingLabel.Text = "Power of move Fling when used with this item: " + ItemFlingPower;

            ItemIdLabel.IsVisible = true;
            ItemNameLabel.IsVisible = true;
            ItemCostLabel.IsVisible = true;
            ItemFlingLabel.IsVisible = true;

            LoadingIcon.IsRunning = false;


        }
    }

}
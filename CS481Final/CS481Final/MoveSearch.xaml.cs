﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using PokeAPI;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481Final
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MoveSearch : ContentPage
	{
		public MoveSearch ()
		{
			InitializeComponent ();
            MoveFind.Keyboard = Keyboard.Create(KeyboardFlags.CapitalizeNone);
		}

        async void RequestMoveData(object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(MoveFind.Text))
            {
                //PokemonFind.Text = oldSearch;
                //PokemonFind.lastSearch = oldSearch;
                await DisplayAlert("Empty Search!", "Move search is empty.", "Close");
                System.Diagnostics.Debug.WriteLine("Empty string");
                return;
            }

            try
            {
                await PullData();
                System.Diagnostics.Debug.WriteLine("got move info!");
            }
            catch (HttpRequestException ex)
            {
                System.Diagnostics.Debug.WriteLine("move not found.");
                await DisplayAlert("Invalid search", "Move not found.", "Close");
                LoadingIcon.IsRunning = false;


            }
        }
        async Task PullData()
        {
            LoadingIcon.IsRunning = true;
            string newMove = MoveFind.Text;

            Move m = await DataFetcher.GetNamedApiObject<Move>(newMove.ToLower());
            IntroLabel.IsVisible = false;
            //InstructionLabel.IsVisible = false;

            string MoveName = m.Name;
            float? MoveAcc = m.Accuracy;
            int? MovePP = m.PP;

            int MovePriority = m.Priority;
            int? MovePower = m.Power;

            NameLabel.Text = "Name: " + MoveName.ToUpper();
            AccLabel.Text = "Accuracy: " + MoveAcc;
            PPLabel.Text = "PP: " + MovePP + "/" + MovePP;
            PriorityLabel.Text = "Priority over other moves (higher the better): " + m.Priority;

            if (m.Power == null)
            {
                PowerLabel.Text = "Move's Power: " + "0";
            }
            else
            {
                PowerLabel.Text = "Move's power: " + m.Power;
            }

            NameLabel.IsVisible = true;
            AccLabel.IsVisible = true;
            PPLabel.IsVisible = true;
            PriorityLabel.IsVisible = true;
            PowerLabel.IsVisible = true;
            //PriorityInfoLabel.IsVisible = true;
            //PriorityInfoLabel.Text = "Note: Priority is based off a value between -8 and 8. See Bulbapedia for more details.";

            LoadingIcon.IsRunning = false;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481Final
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CreditsPage : ContentPage
	{
		public CreditsPage ()
		{
			InitializeComponent ();
		}

        async void OnClickApi(object sender, System.EventArgs e)
        {
            await Browser.OpenAsync("https://pokeapi.co/", BrowserLaunchMode.SystemPreferred);
        }

        async void WrapperButton_Clicked(object sender, EventArgs e)
        {
            await Browser.OpenAsync("https://gitlab.com/PoroCYon/PokeApi.NET", BrowserLaunchMode.SystemPreferred);
        }

        async void BulbapediaButton_Clicked(object sender, EventArgs e)
        {
            await Browser.OpenAsync("https://bulbapedia.bulbagarden.net/wiki/Main_Page", BrowserLaunchMode.SystemPreferred);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481Final
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HomePage : ContentPage
	{
		public HomePage ()
		{
			InitializeComponent ();
        }

        private void MainMenuButton_Clicked(object sender, EventArgs e)
        {
            LoadingIcon.IsRunning = true;
            App.Current.MainPage = new MainPage();
        }
    }
}